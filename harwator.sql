CREATE database dbharwator;
USE dbharwator;
CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS Categoria(
	idcategoria INT NOT NULL AUTO_INCREMENT,
	categoria VARCHAR(20),
	PRIMARY KEY (idcategoria)
);

CREATE TABLE IF NOT EXISTS Tienda(
	idtienda INT NOT NULL AUTO_INCREMENT,
	nombretienda VARCHAR(50) NOT NULL,
	linktienda VARCHAR(255),
	PRIMARY KEY (idtienda)
);

CREATE TABLE IF NOT EXISTS Producto(
	idproducto INT NOT NULL AUTO_INCREMENT,
	idcategoriafk INT,
	producto VARCHAR(255),
	idtiendafk INT,
	precio float,
	url VARCHAR(255),
	idproductolimpio INT,
	fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (idproducto),
	FOREIGN KEY (idtiendafk) REFERENCES Tienda (idtienda) ON DELETE CASCADE,
	FOREIGN KEY (idcategoriafk) REFERENCES Categoria (idcategoria) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Precio(
	idprecio INT NOT NULL AUTO_INCREMENT,
	idproducto INT NOT NULL,
	idtienda INT NOT NULL,
	precio float,
	linkproducto VARCHAR(255),
	PRIMARY KEY (idprecio),
	FOREIGN KEY (idproducto) REFERENCES Producto (idproducto),
	FOREIGN KEY (idtienda) REFERENCES Tienda(idtienda)
);

CREATE TABLE IF NOT EXISTS Placabase (
	idpb INT NOT NULL AUTO_INCREMENT,
	idproductofk INT NOT NULL,
	memoria VARCHAR(20),
	fabricante VARCHAR(50),
	descripcion VARCHAR(1000),
	imagen INT, 
	socket VARCHAR(10),
	chipset	 VARCHAR(10),
	PRIMARY KEY (idpb),
	FOREIGN KEY (idproductofk) REFERENCES Producto(idproducto) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Lectortarjetas (
	idlectortarjetas INT NOT NULL AUTO_INCREMENT,
	idproductofk INT NOT NULL,
	tipo  VARCHAR(50),
	ranuras  INT,
	descripcion VARCHAR(1000),
	imagen INT, 
	marca VARCHAR(50),
	PRIMARY KEY (idlectortarjetas),
	FOREIGN KEY (idproductofk) REFERENCES Producto(idproducto) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Tarjetagrafica (
	idtg INT NOT NULL AUTO_INCREMENT,
	idproductofk INT NOT NULL,
	fabricante VARCHAR(50),
	ensamblador VARCHAR(50),
	imagen INT, 
	descripcion VARCHAR(1000),
	PRIMARY KEY (idtg),
	FOREIGN KEY (idproductofk) REFERENCES Producto(idproducto) ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS Procesador (
	idprocesador INT NOT NULL AUTO_INCREMENT,
	idproductofk INT NOT NULL,
	imagen INT, 
	socket VARCHAR(10),
	fabricante VARCHAR(50),
	descripcion VARCHAR(1000),
	PRIMARY KEY (idprocesador),
	FOREIGN KEY (idproductofk) REFERENCES Producto(idproducto) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Torrecaja(
	idcaja INT NOT NULL AUTO_INCREMENT,
	idproductofk INT NOT NULL,
	fabricante VARCHAR(50),
	tipo VARCHAR(50),
	dimensiones VARCHAR(50),
	ventiladoresincl INT,
	descripcion VARCHAR(1000),
	imagen INT, 
	PRIMARY KEY (idcaja),
	FOREIGN KEY (idproductofk) REFERENCES Producto(idproducto) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Discoduro(
	idhd INT NOT NULL AUTO_INCREMENT,
	idproductofk INT NOT NULL,
	fabricante VARCHAR(50),
	capacidad VARCHAR(50),
	tipo VARCHAR(50),
	velocidad VARCHAR(50),
	imagen INT, 
	descripcion VARCHAR(1000),
	PRIMARY KEY (idhd),
	FOREIGN KEY (idproductofk) REFERENCES Producto(idproducto) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Fuentealimentacion(
	idfuenteAlimentacion INT NOT NULL AUTO_INCREMENT,
	idproductofk INT NOT NULL,
	imagen INT,
	fabricante VARCHAR(50),
	descripcion VARCHAR(1000),
	potencia VARCHAR(20),
	modular BOOLEAN,
	certificacion VARCHAR(10),
	PRIMARY KEY (idFuenteAlimentacion),
	FOREIGN KEY (idproductofk) REFERENCES Producto(idproducto) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Memoriaram(
	idmemoriaram INT NOT NULL AUTO_INCREMENT,
	idproductofk INT NOT NULL,
	imagen INT,
	fabricante VARCHAR(50),
	descripcion VARCHAR(1000),
	memoria VARCHAR(20),
	tipo VARCHAR(20),
	PRIMARY KEY (idmemoriaram),
	FOREIGN KEY (idproductofk) REFERENCES Producto(idproducto) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Disipador(
	iddisipador INT NOT NULL AUTO_INCREMENT,
	idproductofk INT NOT NULL,
	imagen INT,
	fabricante VARCHAR(50),
	descripcion VARCHAR(1000),
	tipo ENUM('Liquido', 'Aire'),
	PRIMARY KEY(iddisipador),
	FOREIGN KEY (idproductofk) REFERENCES Producto(idproducto) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS Lector(
	idlector INT NOT NULL AUTO_INCREMENT,
	idproductofk INT NOT NULL,
	imagen INT,
	tipo ENUM('BluRay', 'DVD'),
	descripcion VARCHAR(1000),
	PRIMARY KEY(idlector),
	FOREIGN KEY (idproductofk) REFERENCES Producto(idproducto) ON DELETE CASCADE
);

INSERT INTO Categoria (categoria) VALUES ('Placabase'),
	('Procesador'),
	('Tarjetagrafica'),
	('Memoriaram'),
	('Discoduro'),
	('Fuentealimentacion'),
	('Disipador'),
	('Lectortarjeta'),
	('Torrecaja');

INSERT INTO Tienda (nombretienda,linktienda) VALUES 
	('PcComponentes', 'www.pccomponentes.com'),
	('Amazon', 'www.amazon.es');