### Instalación

Harwator es un proyecto desarrollado por alumnos de segundo curso de Desarrollo de Aplicaciones Web
Las tecnologias utilizadas para el proyecto que hemos creido necesarias para dar vida a nuestro comparador son:

- Servidor web Apache con sistema operativo Ubuntu 16.04.1 y memoria RAM de 2048mb.
- Librerias e interprete actualizado de lenguaje PHP7.0.
- Base de Datos MySQL.
- Framework PHP Symfony 3, con composer actualizado.
- Librería FOS-UserBundle de Symfony 3.
- Mapeo de Tablas relacionales a base de datos con librería Doctrine, incluída en el framework de Symfony 3.

Para proceder a la replicación del proyecto paso a paso seguiremos el siguiente tutorial.

1. Actualizar el instalador de paquetes de Ubuntu
```sh
$ sudo apt-get update
```
2. Instalación de Apache
```sh
$ sudo apt-get install apache2
```
3. Instalación de PHP y sus componentes
```sh
$ sudo apt-get install php7.0
$ sudo apt-get install php7.0-curl
$ sudo apt-get install php-xml
```
4. Instalación de MySQL
```sh
$ sudo apt-get install mysql-server
```
Recordad el nombre de usuario administrador de la BBDD y el Password,
en nuestro caso es root -> ausias.
Deberemos crear un usuario y password para la base de datos no root.
Entrar como root para crear los nuevos usuarios. 
Reemplazar myuser por el nombre de usuario que querais, y mypass por la contraseña deseada.
Nosotros crearemos un usuario por defecto:
* user : myuser *
* pass : mypass *

```
$ mysql -u “root” -p
CREATE USER 'myuser'@'localhost' IDENTIFIED BY 'mypass';
GRANT ALL PRIVILEGES ON *.* TO 'myuser'@'localhost' WITH GRANT OPTION;
EXIT;
```
La configuración del MySQL requiere añadir una información extra para el servidor.
Habrá que modificar este archivo ubicado en: `/etc/mysql/mysql.conf.d/mysqld.cnf` .

```
[mysqld]
#
# * Basic Settings
#
collation-server     = utf8mb4_general_ci # Replaces utf8_general_ci
character-set-server = utf8mb4            # Replaces utf8
user            = mysql
pid-file        = /var/run/mysqld/mysqld.pid
socket          = /var/run/mysqld/mysqld.sock
```
```sh
$ sudo service mysql restart
```

5. Instalación de Symfony3
```sh
$ sudo mkdir -p /usr/local/bin
$ sudo apt-get install curl
$ sudo curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
$ sudo chmod a+x /usr/local/bin/symfony
$ sudo curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
```
6. Conexión aplicación harwator y permisos
Remplazar "myuser:myuser" por el usuario no root que habeis configurado en la base de datos MySQL.

```sh
$ sudo mkdir -p /var/www/harwator
$ sudo chown myuser:myuser /var/www/harwator
$ cd /var/www
$ git clone https://Nzabala@bitbucket.org/harwator/harwator.git harwator
$ sudo setfacl -R -m u:www-data:rX harwator
$ sudo setfacl -R -m u:www-data:rwX harwator/app/cache harwator/app/logs
$ sudo setfacl -dR -m u:www-data:rwX harwator/app/cache harwator/app/logs
$ getfacl harwator/app/cache
```
7. Eliminamos la barra de symfony para el navegador del cliente

```sh
$ export SYMFONY_ENV=prod
```
8. Instalación de las dependencias del proyecto y librerias adicionales para su correcto funcionamiento

```sh
$ cd harwator
$ composer install --no-dev --optimize-autoloader
```
9. Configuración del acceso a la base de datos para symfony
```sh
$ mysql -u "myuser" -p < harwator.sql
```
```
[harwator/app/config/parameters.yml]
database_host (127.0.0.1): 
database_port (null): 
database_name (symfony): dbharwator
database_user (root): myuser
database_password (null): mypass
```
```
$ php app/console doctrine:schema:validate
$ php app/console doctrine:schema:create
$ php app/console cache:clear --env=prod --no-debug
```
10. Configuración de apache

Abrir el fichero /etc/apache2/sites-enabled/000-default.conf de apache y añadir el siguiente host:

Remplazar "myuser" "mypass" por el usuario base de datos MySQL.
```
<VirtualHost *:80>
    ServerName      harwator
    ServerAlias     www.harwator

    DocumentRoot    "/var/www/harwator/web"
    DirectoryIndex  app.php

    SetEnv SYMFONY__DATABASE__USER     "myuser"
    SetEnv SYMFONY__DATABASE__PASSWORD "mypass"


    <Directory "/var/www/harwator/web">
        AllowOverride None
        Allow from All


<IfModule mod_rewrite.c>
Options -MultiViews
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.*)$ app.php [QSA,L]
</IfModule>


    </Directory>

    <IfModule mod_filter.c>
        AddOutputFilterByType DEFLATE "application/atom+xml" \
                                      "application/javascript" \
                                      "application/json" \
                                      "application/rss+xml" \
                                      "application/x-javascript" \
                                      "application/xhtml+xml" \
                                      "application/xml" \
                                      "image/svg+xml" \
                                      "text/css" \
                                      "text/html" \
                                      "text/javascript" \
                                      "text/plain" \
                                      "text/xml"
    </IfModule>

    <IfModule mod_headers.c>
        Header append Vary User-Agent env=!dont-vary

        ExpiresActive On
        ExpiresDefault "now plus 1 week"
        ExpiresByType image/x-icon "now plus 1 month"
        ExpiresByType image/gif    "now plus 1 month"
        ExpiresByType image/png    "now plus 1 month"
        ExpiresByType image/jpeg   "now plus 1 month"
    </IfModule>

</VirtualHost>

```


Tecnologias web: HTML5, CSS, PHP (symfony), MySQL, jQuery, Bootstrap, Bitbucket, editor Cloud codeTasty, webScraping