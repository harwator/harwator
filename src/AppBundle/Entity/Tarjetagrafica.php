<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tarjetagrafica
 *
 * @ORM\Table(name="Tarjetagrafica")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TarjetagraficatestRepository")
 */
class Tarjetagrafica
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtg", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idtg;

    /**
     * @var int
     *
     * @ORM\Column(name="idproductofk", type="integer")
     */
    private $idproductofk;

     /**
     * @var string
     *
     * @ORM\Column(name="fabricante", type="string", length=255)
     */
    private $fabricante;


     /**
     * @var string
     *
     * @ORM\Column(name="ensamblador", type="string", length=255)
     */
    private $ensamblador;

    /**
     * @var int
     *
     * @ORM\Column(name="imagen", type="integer")
     */
    private $imagen;



     /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion; 



    /**
     * Get idtg
     *
     * @return integer
     */
    public function getIdtg()
    {
        return $this->idtg;
    }

    /**
     * Set idproductofk
     *
     * @param integer $idproductofk
     *
     * @return Tarjetagrafica
     */
    public function setIdproductofk($idproductofk)
    {
        $this->idproductofk = $idproductofk;

        return $this;
    }

    /**
     * Get idproductofk
     *
     * @return integer
     */
    public function getIdproductofk()
    {
        return $this->idproductofk;
    }

    /**
     * Set fabricante
     *
     * @param string $fabricante
     *
     * @return Tarjetagrafica
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;

        return $this;
    }

    /**
     * Get fabricante
     *
     * @return string
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * Set ensamblador
     *
     * @param string $ensamblador
     *
     * @return Tarjetagrafica
     */
    public function setEnsamblador($ensamblador)
    {
        $this->ensamblador = $ensamblador;

        return $this;
    }

    /**
     * Get ensamblador
     *
     * @return string
     */
    public function getEnsamblador()
    {
        return $this->ensamblador;
    }

    /**
     * Set imagen
     *
     * @param integer $imagen
     *
     * @return Tarjetagrafica
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return integer
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Tarjetagrafica
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
