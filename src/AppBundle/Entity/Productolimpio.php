<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Productolimpio
 *
 * @ORM\Entity(readOnly=true)
 * @ORM\Table(name="Productolimpio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductolimpioRepository")
 */
class Productolimpio
{
    /**
     * @var int
     *
     * @ORM\Column(name="idproducto", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idproducto;

    /**
     * @var int
     *
     * @ORM\Column(name="idcategoriafk", type="integer")
     */
    private $idcategoriafk;

    /**
     * @var string
     *
     * @ORM\Column(name="producto", type="string", length=255)
     */
    private $producto;

    /**
     * @var int
     *
     * @ORM\Column(name="idtiendafk", type="integer")
     */
    private $idtiendafk;



    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float")
     */
    private $precio;


    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var int
     *
     * @ORM\Column(name="idproductolimpio", type="integer")
     */
    private $idproductolimpio;





    /**
     * Get idproducto
     *
     * @return integer
     */
    public function getIdproducto()
    {
        return $this->idproducto;
    }

    /**
     * Set idcategoriafk
     *
     * @param integer $idcategoriafk
     *
     * @return Productolimpio
     */
    public function setIdcategoriafk($idcategoriafk)
    {
        $this->idcategoriafk = $idcategoriafk;

        return $this;
    }

    /**
     * Get idcategoriafk
     *
     * @return integer
     */
    public function getIdcategoriafk()
    {
        return $this->idcategoriafk;
    }

    /**
     * Set producto
     *
     * @param string $producto
     *
     * @return Productolimpio
     */
    public function setProducto($producto)
    {
        $this->producto = $producto;

        return $this;
    }

    /**
     * Get producto
     *
     * @return string
     */
    public function getProducto()
    {
        return $this->producto;
    }

    /**
     * Set idtiendafk
     *
     * @param integer $idtiendafk
     *
     * @return Productolimpio
     */
    public function setIdtiendafk($idtiendafk)
    {
        $this->idtiendafk = $idtiendafk;

        return $this;
    }

    /**
     * Get idtiendafk
     *
     * @return integer
     */
    public function getIdtiendafk()
    {
        return $this->idtiendafk;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return Productolimpio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Productolimpio
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set idproductolimpio
     *
     * @param integer $idproductolimpio
     *
     * @return Productolimpio
     */
    public function setIdproductolimpio($idproductolimpio)
    {
        $this->idproductolimpio = $idproductolimpio;

        return $this;
    }

    /**
     * Get idproductolimpio
     *
     * @return integer
     */
    public function getIdproductolimpio()
    {
        return $this->idproductolimpio;
    }
}
