<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Procesador
 *
 * @ORM\Table(name="Procesador")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProcesadorRepository")
 */
class Procesador
{
    /**
     * @var int
     *
     * @ORM\Column(name="idprocesador", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idprocesador;

    /**
     * @var int
     *
     * @ORM\Column(name="idproductofk", type="integer")
     */
    private $idproductofk;


    /**
     * @var int
     *
     * @ORM\Column(name="imagen", type="integer")
     */
    private $imagen;





     /**
     * @var string
     *
     * @ORM\Column(name="socket", type="string", length=255)
     */
    private $socket;





     /**
     * @var string
     *
     * @ORM\Column(name="fabricante", type="string", length=255)
     */
    private $fabricante;


    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion; 


    /**
     * Get idprocesador
     *
     * @return integer
     */
    public function getIdprocesador()
    {
        return $this->idprocesador;
    }

    /**
     * Set idproductofk
     *
     * @param integer $idproductofk
     *
     * @return Procesador
     */
    public function setIdproductofk($idproductofk)
    {
        $this->idproductofk = $idproductofk;

        return $this;
    }

    /**
     * Get idproductofk
     *
     * @return integer
     */
    public function getIdproductofk()
    {
        return $this->idproductofk;
    }

    /**
     * Set imagen
     *
     * @param integer $imagen
     *
     * @return Procesador
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return integer
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set socket
     *
     * @param string $socket
     *
     * @return Procesador
     */
    public function setSocket($socket)
    {
        $this->socket = $socket;

        return $this;
    }

    /**
     * Get socket
     *
     * @return string
     */
    public function getSocket()
    {
        return $this->socket;
    }

    /**
     * Set fabricante
     *
     * @param string $fabricante
     *
     * @return Procesador
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;

        return $this;
    }

    /**
     * Get fabricante
     *
     * @return string
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Procesador
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
