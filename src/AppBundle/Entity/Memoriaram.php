<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Memoriaram
 *
 * @ORM\Table(name="Memoriaram")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MemoriaramRepository")
 */
class Memoriaram
{
    /**
     * @var int
     *
     * @ORM\Column(name="idmemoriaram", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idmemoriaram;

    /**
     * @var int
     *
     * @ORM\Column(name="idproductofk", type="integer")
     */
    private $idproductofk;


    /**
     * @var int
     *
     * @ORM\Column(name="imagen", type="integer")
     */
    private $imagen;




     /**
     * @var string
     *
     * @ORM\Column(name="fabricante", type="string", length=255)
     */
    private $fabricante;


    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion; 

    /**
     * @var string
     *
     * @ORM\Column(name="memoria", type="string", length=20)
     */
    private $memoria; 
    



    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=20)
     */
    private $tipo; 

    /**
     * Get idmemoriaram
     *
     * @return integer
     */
    public function getIdmemoriaram()
    {
        return $this->idmemoriaram;
    }

    /**
     * Set idproductofk
     *
     * @param integer $idproductofk
     *
     * @return Memoriaram
     */
    public function setIdproductofk($idproductofk)
    {
        $this->idproductofk = $idproductofk;

        return $this;
    }

    /**
     * Get idproductofk
     *
     * @return integer
     */
    public function getIdproductofk()
    {
        return $this->idproductofk;
    }

    /**
     * Set imagen
     *
     * @param integer $imagen
     *
     * @return Memoriaram
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return integer
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set fabricante
     *
     * @param string $fabricante
     *
     * @return Memoriaram
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;

        return $this;
    }

    /**
     * Get fabricante
     *
     * @return string
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Memoriaram
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set memoria
     *
     * @param string $memoria
     *
     * @return Memoriaram
     */
    public function setMemoria($memoria)
    {
        $this->memoria = $memoria;

        return $this;
    }

    /**
     * Get memoria
     *
     * @return string
     */
    public function getMemoria()
    {
        return $this->memoria;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Memoriaram
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
