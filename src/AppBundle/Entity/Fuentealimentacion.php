<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Fuentealimentacion
 *
 * @ORM\Table(name="Fuentealimentacion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FuentealimentacionRepository")
 */
class Fuentealimentacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="idfuenteAlimentacion", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idfuenteAlimentacion;

    /**
     * @var int
     *
     * @ORM\Column(name="idproductofk", type="integer")
     */
    private $idproductofk;


    /**
     * @var int
     *
     * @ORM\Column(name="imagen", type="integer")
     */
    private $imagen;


    /**
     * @var string
     *
     * @ORM\Column(name="fabricante", type="string", length=255)
     */
    private $fabricante;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion; 



    /**
     * @var string
     *
     * @ORM\Column(name="potencia", type="string", length=255)
     */
    private $potencia; 


    /**
     * @var boolean
     *
     * @ORM\Column(name="modular", type="boolean")
     */
    private $modular; 


    /**
     * @var string
     *
     * @ORM\Column(name="certificacion", type="string", length=255)
     */
    private $certificacion; 




    /**
     * Get idfuenteAlimentacion
     *
     * @return integer
     */
    public function getIdfuenteAlimentacion()
    {
        return $this->idfuenteAlimentacion;
    }

    /**
     * Set idproductofk
     *
     * @param integer $idproductofk
     *
     * @return Fuentealimentacion
     */
    public function setIdproductofk($idproductofk)
    {
        $this->idproductofk = $idproductofk;

        return $this;
    }

    /**
     * Get idproductofk
     *
     * @return integer
     */
    public function getIdproductofk()
    {
        return $this->idproductofk;
    }

    /**
     * Set imagen
     *
     * @param integer $imagen
     *
     * @return Fuentealimentacion
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return integer
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set fabricante
     *
     * @param string $fabricante
     *
     * @return Fuentealimentacion
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;

        return $this;
    }

    /**
     * Get fabricante
     *
     * @return string
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Fuentealimentacion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set potencia
     *
     * @param string $potencia
     *
     * @return Fuentealimentacion
     */
    public function setPotencia($potencia)
    {
        $this->potencia = $potencia;

        return $this;
    }

    /**
     * Get potencia
     *
     * @return string
     */
    public function getPotencia()
    {
        return $this->potencia;
    }

    /**
     * Set modular
     *
     * @param boolean $modular
     *
     * @return Fuentealimentacion
     */
    public function setModular($modular)
    {
        $this->modular = $modular;

        return $this;
    }

    /**
     * Get modular
     *
     * @return boolean
     */
    public function getModular()
    {
        return $this->modular;
    }

    /**
     * Set certificacion
     *
     * @param string $certificacion
     *
     * @return Fuentealimentacion
     */
    public function setCertificacion($certificacion)
    {
        $this->certificacion = $certificacion;

        return $this;
    }

    /**
     * Get certificacion
     *
     * @return string
     */
    public function getCertificacion()
    {
        return $this->certificacion;
    }
}
