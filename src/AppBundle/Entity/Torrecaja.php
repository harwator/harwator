<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Torrecaja
 *
 * @ORM\Table(name="Torrecaja")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TorrecajaRepository")
 */
class Torrecaja
{
    /**
     * @var int
     *
     * @ORM\Column(name="idcaja", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idcaja;

    /**
     * @var int
     *
     * @ORM\Column(name="idproductofk", type="integer")
     */
    private $idproductofk;

    /**
     * @var string
     *
     * @ORM\Column(name="fabricante", type="string", length=255)
     */
    private $fabricante;


    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=20)
     */
    private $tipo; 

    /**
     * @var string
     *
     * @ORM\Column(name="dimensiones", type="string", length=255)
     */
    private $dimensiones; 


    /**
     * @var int
     *
     * @ORM\Column(name="ventiladoresincl", type="integer")
     */
    private $ventiladoresincl;


    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion; 


    /**
     * @var int
     *
     * @ORM\Column(name="imagen", type="integer")
     */
    private $imagen;




    /**
     * Get idcaja
     *
     * @return integer
     */
    public function getIdcaja()
    {
        return $this->idcaja;
    }

    /**
     * Set idproductofk
     *
     * @param integer $idproductofk
     *
     * @return Torrecaja
     */
    public function setIdproductofk($idproductofk)
    {
        $this->idproductofk = $idproductofk;

        return $this;
    }

    /**
     * Get idproductofk
     *
     * @return integer
     */
    public function getIdproductofk()
    {
        return $this->idproductofk;
    }

    /**
     * Set fabricante
     *
     * @param string $fabricante
     *
     * @return Torrecaja
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;

        return $this;
    }

    /**
     * Get fabricante
     *
     * @return string
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Torrecaja
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set dimensiones
     *
     * @param string $dimensiones
     *
     * @return Torrecaja
     */
    public function setDimensiones($dimensiones)
    {
        $this->dimensiones = $dimensiones;

        return $this;
    }

    /**
     * Get dimensiones
     *
     * @return string
     */
    public function getDimensiones()
    {
        return $this->dimensiones;
    }

    /**
     * Set ventiladoresincl
     *
     * @param integer $ventiladoresincl
     *
     * @return Torrecaja
     */
    public function setVentiladoresincl($ventiladoresincl)
    {
        $this->ventiladoresincl = $ventiladoresincl;

        return $this;
    }

    /**
     * Get ventiladoresincl
     *
     * @return integer
     */
    public function getVentiladoresincl()
    {
        return $this->ventiladoresincl;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Torrecaja
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set imagen
     *
     * @param integer $imagen
     *
     * @return Torrecaja
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return integer
     */
    public function getImagen()
    {
        return $this->imagen;
    }
}
