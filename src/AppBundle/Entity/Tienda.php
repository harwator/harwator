<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tienda
 *
 * @ORM\Table(name="Tienda")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TiendaRepository")
 */
class Tienda
{
    /**
     * @var int
     *
     * @ORM\Column(name="idtienda", type="integer")
     * @ORM\Id
     */
    private $idTienda;

    /**
	 * @var string
	 *
	 * @ORM\Column(name="nombretienda", type="string", length=50)
     */
    private $nombretienda;

    /**
	 * @var string
	 *
	 * @ORM\Column(name="linktienda", type="string", length=255)
     */
    private $linktienda;

    /**
     * Set idTienda
     *
     * @param integer $idTienda
     *
     * @return Tienda
     */
    public function setIdTienda($idTienda)
    {
        $this->idTienda = $idTienda;

        return $this;
    }

    /**
     * Get idTienda
     *
     * @return integer
     */
    public function getIdTienda()
    {
        return $this->idTienda;
    }

    /**
     * Set nombretienda
     *
     * @param \string $nombretienda
     *
     * @return Tienda
     */
    public function setNombretienda($nombretienda)
    {
        $this->nombretienda = $nombretienda;

        return $this;
    }

    /**
     * Get nombretienda
     *
     * @return \string
     */
    public function getNombretienda()
    {
        return $this->nombretienda;
    }

    /**
     * Set linktienda
     *
     * @param \string $linktienda
     *
     * @return Tienda
     */
    public function setLinktienda($linktienda)
    {
        $this->linktienda = $linktienda;

        return $this;
    }

    /**
     * Get linktienda
     *
     * @return \string
     */
    public function getLinktienda()
    {
        return $this->linktienda;
    }
}
