<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Placabase
 *
 * @ORM\Table(name="Placabase")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlacabaseRepository")
 */
class Placabase
{
    /**
     * @var int
     *
     * @ORM\Column(name="idpb", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idpb;

    /**
     * @var int
     *
     * @ORM\Column(name="idproductofk", type="integer")
     */
    private $idproductofk;




     /**
     * @var string
     *
     * @ORM\Column(name="memoria", type="string", length=255)
     */
    private $memoria;





     /**
     * @var string
     *
     * @ORM\Column(name="fabricante", type="string", length=255)
     */
    private $fabricante;


    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion; 

    /**
     * @var int
     *
     * @ORM\Column(name="imagen", type="integer")
     */
    private $imagen;


    /**
     * @var string
     *
     * @ORM\Column(name="socket", type="string", length=255)
     */
    private $socket;


    /**
     * @var string
     *
     * @ORM\Column(name="chipset", type="string", length=255)
     */
    private $chipset;





    /**
     * Get idpb
     *
     * @return integer
     */
    public function getIdpb()
    {
        return $this->idpb;
    }

    /**
     * Set idproductofk
     *
     * @param integer $idproductofk
     *
     * @return PlacaBase
     */
    public function setIdproductofk($idproductofk)
    {
        $this->idproductofk = $idproductofk;

        return $this;
    }

    /**
     * Get idproductofk
     *
     * @return integer
     */
    public function getIdproductofk()
    {
        return $this->idproductofk;
    }

    /**
     * Set memoria
     *
     * @param string $memoria
     *
     * @return PlacaBase
     */
    public function setMemoria($memoria)
    {
        $this->memoria = $memoria;

        return $this;
    }

    /**
     * Get memoria
     *
     * @return string
     */
    public function getMemoria()
    {
        return $this->memoria;
    }

    /**
     * Set fabricante
     *
     * @param string $fabricante
     *
     * @return PlacaBase
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;

        return $this;
    }

    /**
     * Get fabricante
     *
     * @return string
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return PlacaBase
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set imagen
     *
     * @param integer $imagen
     *
     * @return PlacaBase
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return integer
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set socket
     *
     * @param string $socket
     *
     * @return PlacaBase
     */
    public function setSocket($socket)
    {
        $this->socket = $socket;

        return $this;
    }

    /**
     * Get socket
     *
     * @return string
     */
    public function getSocket()
    {
        return $this->socket;
    }

    /**
     * Set chipset
     *
     * @param string $chipset
     *
     * @return PlacaBase
     */
    public function setChipset($chipset)
    {
        $this->chipset = $chipset;

        return $this;
    }

    /**
     * Get chipset
     *
     * @return string
     */
    public function getChipset()
    {
        return $this->chipset;
    }
}
