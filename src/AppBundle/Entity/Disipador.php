<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Disipador
 *
 * @ORM\Table(name="Disipador")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DisipadorRepository")
 */
class Disipador
{
    /**
     * @var int
     *
     * @ORM\Column(name="iddisipador", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $iddisipador;

    /**
     * @var int
     *
     * @ORM\Column(name="idproductofk", type="integer")
     */
    private $idproductofk;


    /**
     * @var int
     *
     * @ORM\Column(name="imagen", type="integer")
     */
    private $imagen;


    /**
     * @var string
     *
     * @ORM\Column(name="fabricante", type="string", length=255)
     */
    private $fabricante;


    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion; 




    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=20)
     */
    private $tipo; 


    /**
     * Get iddisipador
     *
     * @return integer
     */
    public function getIddisipador()
    {
        return $this->iddisipador;
    }

    /**
     * Set idproductofk
     *
     * @param integer $idproductofk
     *
     * @return Disipador
     */
    public function setIdproductofk($idproductofk)
    {
        $this->idproductofk = $idproductofk;

        return $this;
    }

    /**
     * Get idproductofk
     *
     * @return integer
     */
    public function getIdproductofk()
    {
        return $this->idproductofk;
    }

    /**
     * Set imagen
     *
     * @param integer $imagen
     *
     * @return Disipador
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return integer
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set fabricante
     *
     * @param string $fabricante
     *
     * @return Disipador
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;

        return $this;
    }

    /**
     * Get fabricante
     *
     * @return string
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Disipador
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Disipador
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
