<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Discoduro
 *
 * @ORM\Table(name="Discoduro")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DiscoduroRepository")
 */
class Discoduro
{
    /**
     * @var int
     *
     * @ORM\Column(name="idhd", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idhd;

    /**
     * @var int
     *
     * @ORM\Column(name="idproductofk", type="integer")
     */
    private $idproductofk;


    /**
     * @var string
     *
     * @ORM\Column(name="fabricante", type="string", length=255)
     */
    private $fabricante;


    /**
     * @var string
     *
     * @ORM\Column(name="capacidad", type="string", length=20)
     */
    private $capacidad; 




    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=20)
     */
    private $tipo; 



    /**
     * @var string
     *
     * @ORM\Column(name="velocidad", type="string", length=20)
     */
    private $velocidad; 




    /**
     * @var int
     *
     * @ORM\Column(name="imagen", type="integer")
     */
    private $imagen;






    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion; 


    


    /**
     * Get idhd
     *
     * @return integer
     */
    public function getIdhd()
    {
        return $this->idhd;
    }

    /**
     * Set idproductofk
     *
     * @param integer $idproductofk
     *
     * @return Discoduro
     */
    public function setIdproductofk($idproductofk)
    {
        $this->idproductofk = $idproductofk;

        return $this;
    }

    /**
     * Get idproductofk
     *
     * @return integer
     */
    public function getIdproductofk()
    {
        return $this->idproductofk;
    }

    /**
     * Set fabricante
     *
     * @param string $fabricante
     *
     * @return Discoduro
     */
    public function setFabricante($fabricante)
    {
        $this->fabricante = $fabricante;

        return $this;
    }

    /**
     * Get fabricante
     *
     * @return string
     */
    public function getFabricante()
    {
        return $this->fabricante;
    }

    /**
     * Set capacidad
     *
     * @param string $capacidad
     *
     * @return Discoduro
     */
    public function setCapacidad($capacidad)
    {
        $this->capacidad = $capacidad;

        return $this;
    }

    /**
     * Get capacidad
     *
     * @return string
     */
    public function getCapacidad()
    {
        return $this->capacidad;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     *
     * @return Discoduro
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set velocidad
     *
     * @param string $velocidad
     *
     * @return Discoduro
     */
    public function setVelocidad($velocidad)
    {
        $this->velocidad = $velocidad;

        return $this;
    }

    /**
     * Get velocidad
     *
     * @return string
     */
    public function getVelocidad()
    {
        return $this->velocidad;
    }

    /**
     * Set imagen
     *
     * @param integer $imagen
     *
     * @return Discoduro
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return integer
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Discoduro
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
}
