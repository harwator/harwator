<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query\Expr\Join;

class DefaultController extends Controller
{
   /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
    

       $categorias = $this->getDoctrine()
        ->getRepository('AppBundle:Categoria')
        ->findAll();

    $em = $this->getDoctrine()->getManager();


	//obtener descripción, precio, categoria, imagen de los 10 primeros productos de cada categoria
	//tarjetasgraficas
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->innerJoin('AppBundle:Tarjetagrafica','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$tarjetasgraficas = $query->getResult();

	//placasbase
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->innerJoin('AppBundle:Placabase','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$placasbase = $query->getResult();

	//procesadores
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->innerJoin('AppBundle:Procesador','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$procesadores = $query->getResult();

	//memoriaram
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->innerJoin('AppBundle:Memoriaram','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$memoriasram = $query->getResult();

	//discosduros
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->innerJoin('AppBundle:Discoduro','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$discosduros = $query->getResult();

	//fuentesalimentacion
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->innerJoin('AppBundle:Fuentealimentacion','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$fuentesalimentacion = $query->getResult();


	//disipadores
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->innerJoin('AppBundle:Disipador','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$disipadores = $query->getResult();


	//torrescajas
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->innerJoin('AppBundle:Torrecaja','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$torrescajas = $query->getResult();

	   $productos = array_merge($tarjetasgraficas, 
	   							$placasbase, 
	   							$procesadores, 
	   							$memoriasram,
	   							$discosduros,
	   							$fuentesalimentacion,
	   							$disipadores,
	   							$torrescajas
	   							);

       return $this->render('default/index.html.twig', array(
            'productos' => $productos,
            'categorias' => $categorias,
            'categoria' => "Todas las Categorias"
            ));
    }

    /**
     * @Route("/categoria/", name="categoria")
     */
    public function indexCategoryAction(Request $request)
    {
 		$categoria = $request->get('categoria');

 		$em = $this->getDoctrine()->getManager();

 		$objetocategoria = $this->getDoctrine()
        ->getRepository('AppBundle:Categoria')
        ->findOneByCategoria($categoria);

        $idcategoriafk = $objetocategoria->getIdcategoria();

       	$categorias = $this->getDoctrine()
        ->getRepository('AppBundle:Categoria')
        ->findAll();

        $lista_productos = $em->createQueryBuilder();
        $lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','c.imagen')
			->from('AppBundle:Productolimpio','p')
			->innerJoin('AppBundle:'.ucfirst($categoria),'c','WITH','p.idproducto = c.idproductofk')
			->where('p.idcategoriafk ='.$idcategoriafk)
			->orderBy('p.idproductolimpio','DESC');

		$query = $lista_productos->getQuery();
		$productos = $query->getResult();

       return $this->render('default/index.html.twig', array(
            'productos' => $productos,
            'categorias' => $categorias,
            'categoria' => $objetocategoria->getCategoria()
            ));
    }
}
