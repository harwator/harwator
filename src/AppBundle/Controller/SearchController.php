<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query\Expr\Join;

class SearchController extends Controller
{
   /**
     * @Route("/search", name="Busqueda")
     */
    public function indexAction(Request $request)
    {

       $busqueda=$_GET["search"];

       $categorias = $this->getDoctrine()
        ->getRepository('AppBundle:Categoria')
        ->findAll();


 $em = $this->getDoctrine()->getManager();




$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->where('p.producto like :busqueda')
			->setParameter('busqueda', '%'.$busqueda.'%')
			->innerJoin('AppBundle:Tarjetagrafica','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$tarjetasgraficas = $query->getResult();

	//placasbase
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->where('p.producto like :busqueda')
			->setParameter('busqueda', '%'.$busqueda.'%')
			->innerJoin('AppBundle:Placabase','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$placasbase = $query->getResult();

	//procesadores
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->where('p.producto like :busqueda')
			->setParameter('busqueda', '%'.$busqueda.'%')
			->innerJoin('AppBundle:Procesador','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$procesadores = $query->getResult();

	//memoriaram
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->where('p.producto like :busqueda')
			->setParameter('busqueda', '%'.$busqueda.'%')
			->innerJoin('AppBundle:Memoriaram','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$memoriasram = $query->getResult();

	//discosduros
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->where('p.producto like :busqueda')
			->setParameter('busqueda', '%'.$busqueda.'%')
			->innerJoin('AppBundle:Discoduro','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$discosduros = $query->getResult();

	//fuentesalimentacion
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->where('p.producto like :busqueda')
			->setParameter('busqueda', '%'.$busqueda.'%')
			->innerJoin('AppBundle:Fuentealimentacion','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$fuentesalimentacion = $query->getResult();


	//disipadores
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->where('p.producto like :busqueda')
			->setParameter('busqueda', '%'.$busqueda.'%')
			->innerJoin('AppBundle:Disipador','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$disipadores = $query->getResult();


	//torrescajas
	$lista_productos = $em->createQueryBuilder();
	$lista_productos->select('p.idproducto, p.producto','p.precio','p.idcategoriafk','t.imagen')
			->from('AppBundle:Productolimpio','p')
			->where('p.producto like :busqueda')
			->setParameter('busqueda', '%'.$busqueda.'%')
			->innerJoin('AppBundle:Torrecaja','t','WITH','p.idproducto = t.idproductofk')
			->orderBy("p.idproductolimpio","DESC")
			->setMaxResults(10);

	$query = $lista_productos->getQuery();
	$torrescajas = $query->getResult();

	   $productos = array_merge($tarjetasgraficas, 
	   							$placasbase, 
	   							$procesadores, 
	   							$memoriasram,
	   							$discosduros,
	   							$fuentesalimentacion,
	   							$disipadores,
	   							$torrescajas
	   							);

	   if (!($productos)){
	   	return $this->render('default/notfound.html.twig', array(
            'productos' => $productos,
            'categorias' => $categorias,
            'categoria' => "Ups..."
            ));
	   }

       return $this->render('default/index.html.twig', array(
            'productos' => $productos,
            'categorias' => $categorias,
            'categoria' => "Resultados de búsqueda"
            ));
    }
    
}

//copyright by sintox
