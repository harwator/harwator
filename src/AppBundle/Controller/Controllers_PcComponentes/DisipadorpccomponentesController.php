<?php

namespace AppBundle\Controller\Controllers_PcComponentes;

use AppBundle\Entity\Producto;
use AppBundle\Repository\ProductotestRespository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\AppFunctions\Functions;
use AppBundle\Entity\Disipador;

class DisipadorpccomponentesController extends Controller
{
    /**
     * @Route("/Disipadorpccomponentes", name="Disipadorpccomponentes")
     */
    public function indexAction(Request $request)
    {
        $get_Productos_bd = $this->getDoctrine()
        ->getRepository('AppBundle:Producto')
        ->findBy(array('idcategoriafk'=>7,'idtiendafk'=>1));
        

        
        $idultimopro = $this->getDoctrine()->getManager()
        ->createQuery('SELECT p FROM AppBundle:Producto p ORDER BY p.idproducto DESC')
        ->setMaxResults(1)
        ->getResult();
        

        if (!empty($idultimopro)){
         $ultima_id= $idultimopro[0]->getIdproducto() + 1;
     }else{$ultima_id=1;}
     $ultima_id2=$ultima_id;


     $em               = $this->getDoctrine()->getManager();


     $productos_nuevos = "";
     foreach ($get_Productos_bd as $producto) {
        $productos_bd[] = $producto->getProducto();
        $idproducto_bd[]=$producto->getIdProducto();

    }


    $i = 0;
    $j = 0;
    $semaforo=0;

    for ($vuelta = 0; $vuelta <= 20; $vuelta++) {




            $html = file_get_contents('https://www.pccomponentes.com/listado/ajax?page='.$vuelta.'&order=relevance&gtmTitle=Ventiladores%20CPU&idFamilies%5B%5D=154'); //get the html returned from the followvueltag

            $pccomponentes_doc = new \DOMDocument();

            libxml_use_internal_errors(true); //disable libxml errors

            if (!empty($html)) {
                //if any html is actually returned

                $pccomponentes_doc->loadHTML($html);
                libxml_clear_errors(); //remove errors for yucky html

                $pccomponentes_xpath = new \DOMXPath($pccomponentes_doc);

                //get all the h2's with an id

                $pccomponentes_row = $pccomponentes_xpath->query('//a[@class="GTM-productClick enlace-disimulado"]');
                $precio_row        = $pccomponentes_xpath->query('//div[@class="tarjeta-articulo__precio-actual"]');
                // $link_row = $pccomponentes_xpath->query('//a[@itemprop="url"]');

                if ($pccomponentes_row->length > 0) {
                    $semaforo=0;
                    for ($z=0; $z <$pccomponentes_row->length ; $z++) { 
                        if (strpos($pccomponentes_row[$z]->nodeValue, 'Reacondicionado')==false){
                            $productos_web_nombre[] = $pccomponentes_row[$z]->nodeValue;
                            $precio= explode("â",$precio_row[$z]->nodeValue);
                            $precios[] = $precio[0];
                        }


                    }
                }


          //      foreach ($pccomponentes_doc->getElementsByTagName('a') as $node) {
                foreach ($pccomponentes_doc->getElementsByTagName('h3') as $node) {
                    foreach ($node->getElementsByTagName('a') as $link) {
                        if (strpos($link->nodeValue, 'Reacondicionado')==false){
                            $stringlink = $link->getAttribute('href');
                            $Link[]     = "https://www.pccomponentes.com" . $stringlink;
                        }
                    }

                }


                foreach ($pccomponentes_doc->getElementsByTagName('img') as $img) {
                    $temp=$img->getAttribute('alt');
                    if (strpos($temp,'Reacondicionado')==false){
                     $foto[] = "https:".$img->getAttribute('src');




                 }

             }
         }
     }

 $producto_existente = null;


     if (isset($productos_bd) && count($productos_bd) > 0) {}
        else {
            $productos_bd       = null;
           }




    //recorre el array productos web nombre y si encuentra una coincidencia el semaforo se pone a 1
    //y actualiza el precio, si el semaforo no cambia el producto es nuevo y lo inserta

     $count = 0;
            for ($t = 0; $t < count($productos_web_nombre); $t++) {

                for ($r = 0; $r < count($productos_bd); $r++) {

                    if (strpos($productos_bd[$r], $productos_web_nombre[$t]) !== false) {
                        $producto_existente[] = $productos_bd[$r];

                    //update producto

                    //update link y precio


                        $price=str_replace(".","", $precios[$t]);
                        $price=str_replace(",",".", $price);

                        $query=$em->createQuery("update AppBundle:Producto p SET p.precio='".floatval($price)."' where p.producto='".$productos_bd[$r]."'");
                        $result=$query->getResult();


                        $count++;



                    }}

                    if ($count == 0) {
                        //insert producto nuevo

                        $price=str_replace(".","", $precios[$t]);
                        $price=str_replace(",",".", $price);
                        $productos_nuevos[] = $productos_web_nombre[$t];
                    //insert producto nuevo
                        $productoinsert = new Producto();
                        $productoinsert->setProducto($productos_web_nombre[$t]);
                        $productoinsert->setPrecio(floatval($price));
                        $productoinsert-> setIdtiendafk(1);
                        $productoinsert->setUrl($Link[$t]);
                        $productoinsert->setidcategoriafk(7);
                        $em->persist($productoinsert);


                     $disipador= new Disipador();



                        // $ensamblador= explode(" ",$productos_web_nombre[$t]);
                        // print_r($ensamblador);
                        //  $disipador->setEnsamblador($ensamblador[0]);

                     $disipador->setIdproductofk($ultima_id);
                     $disipador->setImagen($ultima_id);
                     $fabricante= explode(" ",$productos_web_nombre[$t]);
                     $disipador->setFabricante($fabricante[0]);
                     $ultima_id++;
                     $em->persist($disipador);

                     $function= new Functions();
                     $function->descargarImagen($foto[$t], $ultima_id2.".jpg", './img/productosimg/');
                     $ultima_id2++;
                    }

                }











          //  print_r( $productos_bd);
       // if (isset($foto)){
         //       print_r($producto_existente);
         //       echo "//////////////////////////////";
         //       print_r($productos_nuevos);
       //     print_r($productos_web_nombre);
      //  }
                $em->flush();
//print_r($productos_bd);

                return $this->render('admin/Genericapccomponentes.html.twig', array(
                    'nombre'             => $productos_web_nombre,
                    'precio'             => $precios,
                    'link'               => $Link,
                    'productos_nuevos'   => $productos_nuevos,
                    'producto_existente' => $producto_existente,
                    'get_Productos_bd'   => $productos_bd,
                    ));
            }
        }


//@copyright sintox



