<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Producto;
use AppBundle\Repository\ProductotestRespository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class CompareController extends Controller
{
	/**
	 * @Route("/comparar", name="comparar")
	 */
	public function indexAction(Request $request)
	{
		$post = Request::createFromGlobals();
		$productosT1 = "";
		$productosT2 = "";
		$recomendado = "";

		if ($post->request->has('submit')) {
			$resultados = $post->request->all();
			$tipoProducto = $resultados['tipo'];
			$tienda1 = $resultados['tienda1'];
			$tienda2 = $resultados['tienda2'];
			if ($tienda2 === $tienda1) {
				echo "ERROR: No selecciones dos tiendas iguales.";
			}
			else {
				$em = $this->getDoctrine()->getManager();
				$query=$em->createQuery("SELECT p FROM AppBundle:Producto p WHERE p.idcategoriafk=".$tipoProducto." AND p.idtiendafk=".$tienda1." AND p.idproductolimpio IS NULL");
				$resultsTienda1=$query->getResult();
				$em->flush();
				$query=$em->createQuery("SELECT p FROM AppBundle:Producto p WHERE p.idcategoriafk=".$tipoProducto." AND p.idtiendafk=".$tienda2." AND p.idproductolimpio IS NULL");
				$resultsTienda2=$query->getResult();
				$em->flush();
				foreach ($resultsTienda1 as $resultTienda1) {
					$productosT1[] = $resultTienda1->getProducto();
				}
				foreach ($resultsTienda2 as $resultTienda2) {
					$productosT2[] = $resultTienda2->getProducto();
				}
				for ($k=0; $k < count($productosT1); $k++) { 
					for ($l=0; $l < count($productosT2); $l++) { 
						similar_text(trim($productosT1[$k]), trim($productosT2[$l]), $percent);
						if ($percent>80) {
							$recomendado[] = array(
								'resultT1' => $productosT1[$k], 
								'resultT2' => $productosT2[$l], 
								'percent' => $percent,
								);
						}
						
					}
				}
			}
		} 
		return $this->render('default/comparar.html.twig', array('productosT1' => $productosT1, 'productosT2' => $productosT2, 'recomendados' => $recomendado));
	}

	/**
	* @Route("/productosIguales", name="productosIguales")
    * @return \Symfony\Component\HttpFoundation\Response
	*/
	public function productosIguales(Request $request)
	{

		$productosIguales= $request->request->get('productos');

    	$response = new Response('Ok!');
    	
    	$em = $this->getDoctrine()->getManager();

    	for ($i=0; $i < count($productosIguales); $i++) { 
    		for ($j=0; $j < 2; $j++) { 	
    			$producto = $this->getDoctrine()
    			->getRepository('AppBundle:Producto')
    			->findOneByProducto($productosIguales[$i][$j]);

    			if ($producto->getIdtiendafk() == 1) {
    				$idProducto = $producto->getIdproducto();
    			}

    			$query=$em->createQuery("update AppBundle:Producto p SET p.idproductolimpio='".$idProducto."' where p.producto='".$productosIguales[$i][$j]."'");
    			$result=$query->getResult();
    			$em->flush();
    		}}
    		return $response;
    	}
    }

    ?>