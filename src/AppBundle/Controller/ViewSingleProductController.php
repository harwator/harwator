<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Categoria;
use AppBundle\Entity\Discoduro;
use AppBundle\Entity\Disipador;
use AppBundle\Entity\Fuentealimentacion;
use AppBundle\Entity\Memoriaram;
use AppBundle\Entity\Placabase;
use AppBundle\Entity\Producto;
use AppBundle\Entity\Tarjetagrafica;
use AppBundle\Entity\Torrecaja;


class ViewSingleProductController extends Controller
{
    /**
     * @Route("/producto/", name="producto")
     */
    public function indexAction(Request $request)
    {
        $idProducto = $request->get('producto');
        $categorias = $this->getDoctrine()
        ->getRepository('AppBundle:Categoria')
        ->findAll();
        $producto = $this->getDoctrine()
        ->getRepository('AppBundle:Producto')
        ->findOneByIdproducto($idProducto);

        $idProductoLimpio = $producto->getIdproductolimpio();

        $productosREF = $this->getDoctrine()
        ->getRepository('AppBundle:Producto')
        ->findByIdproductolimpio($idProducto);
        if(!empty($productosREF)){
            foreach ($productosREF as $productoREF) {
                $id = $productoREF->getIdproducto();
                $categoria = $productoREF->getIdcategoriafk();
                $precio = $productoREF->getPrecio();
                $tienda = $productoREF->getIdtiendafk();
                $url = $productoREF->getUrl();
                if ($tienda === 1) {
                    $nombre = $productoREF->getProducto();    
                }

                //Sacar el nombre de la tienda
                $tienda = $this->getDoctrine()
                ->getRepository('AppBundle:Tienda')
                ->findOneByIdTienda($tienda);
                $nombreTienda = $tienda->getNombretienda();

                //Sacar categoria
                $nombreCategoria = $this->getDoctrine()
                ->getRepository('AppBundle:Categoria')
                ->findOneByIdcategoria($categoria);
                $nombreCategoria = $nombreCategoria->getCategoria();
                $nombreCategoria = "AppBundle:".$nombreCategoria;


                 //Sacar la foto
                $foto = $this->getDoctrine()
                ->getRepository($nombreCategoria)
                ->findOneByIdproductofk($id);
                if(isset($foto)){
                    $fotoProducto = $foto->getImagen();        
                }
                $datos[] = array(
                    "tienda" => $nombreTienda,
                    "precio" => $precio,
                    "url" => $url,
                    );
            }
        }
        else{
            $id = $producto->getIdproducto();
            $nombre = $producto->getProducto();
            $categoria = $producto->getIdcategoriafk();
            $precio = $producto->getPrecio();
            $tienda = $producto->getIdtiendafk();
            $url = $producto->getUrl();

            //Sacar el nombre de la tienda
                $tienda = $this->getDoctrine()
                ->getRepository('AppBundle:Tienda')
                ->findOneByIdTienda($tienda);
                $nombreTienda = $tienda->getNombretienda();

            //Sacar categoria
                $nombreCategoria = $this->getDoctrine()
                ->getRepository('AppBundle:Categoria')
                ->findOneByIdcategoria($categoria);
                $nombreCategoria = $nombreCategoria->getCategoria();
                $nombreCategoria = "AppBundle:".$nombreCategoria;

            //Sacar la foto
            $foto = $this->getDoctrine()
            ->getRepository($nombreCategoria)
            ->findOneByIdproductofk($id);
            $fotoProducto = $foto->getImagen();

            $datos[] = array(
                    "tienda" => $nombreTienda,
                    "precio" => $precio,
                    "url" => $url,
                    );
       }
        return $this->render('default/singleProduct.html.twig', array('nombre' => $nombre, 'imagen' => $fotoProducto, 'datos' => $datos, 'categorias' => $categorias));
    }
}