<?php

namespace AppBundle\Controller\Controllers_Amazon;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Repository\productotestRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Producto;
use simple_html_dom;

class AmazonController extends Controller
{
    /**
     * @Route("/amazon", name="amazon")
     */
    public function indexAction(Request $request)
    {
        for ($page=1; $page < 40; $page++) { //FOR(1)
            $html = file_get_contents('https://www.amazon.es/s/ref=sr_pg_'.$page.'?fst=as%3Aoff&rh=n%3A599370031%2Cn%3A683279031%2Cn%3A937912031%2Cn%3A937935031%2Ck%3Atarjeta+gr%C3%A1fica%2Cp_n_feature_keywords_three_browse-bin%3A4542974031|4542975031%2Cp_6%3AA1AT7YVPFBWXBL&page='.$page.'&keywords=tarjeta+gr%C3%A1fica&ie=UTF8&qid=1491465429'); //HTML de la pàgina
            //Se añade "\" para que llame a la clase nativa
            $amazon_doc = new \DOMDocument();
            libxml_use_internal_errors(TRUE);
            sleep(10);
            //Comprovación de que $html tenga contenido
            if(!empty($html)){ //IF(1)

                //Carga html
                $amazon_doc->loadHTML($html);
                libxml_clear_errors();
                
                //Se añade "\" para que llame a la clase nativa
                $amazon_xpath = new \DOMXPath($amazon_doc);

                //Ruta nombre productos
                $amazon_row = $amazon_xpath->query('//h2[@data-attribute]');
                
                //Ruta precio productos
                $amazon_row_price = $amazon_xpath->query('//span[@class="a-size-base a-color-price s-price a-text-bold"]');

                //Ruta enlaces productos
                $amazon_links = $amazon_xpath->query('//a[@class="a-link-normal s-access-detail-page  s-color-twister-title-link a-text-normal"]/@href');

                //Comprovación de que $amazon_row tenga contenido
                if($amazon_row->length > 0){ //IF(2)
                    foreach($amazon_row as $row){ //FOREACH(1)
                        $result = $row->nodeValue;
                        //Comprobar si el nombre contiene - Tarjeta gráfica, con y sin tilde para quitarlo y guardar solo con el nombre
                        if (strpos($result, '- Tarjeta grafica') !== false){ //IF(3)
                            $finalResult = explode("- Tarjeta grafica", $result);
                            $names[] = $finalResult[0];
                        } //END IF(3)
                        else if( strpos($result, '- Tarjeta gráfica') !== false){
                            $finalResult = explode("- Tarjeta gráfica", $result);
                            $names[] = $finalResult[0];
                        } //END ELSE IF
                        else if(strpos($result, '- Placa') !== false){}
                        else {
                            $names[] = $result;
                        } //END ELSE
                    } //END FOREACH(1)

                    //Guardar todos los precios de los productos
                    foreach($amazon_row_price as $row_price){ //FOREACH(2)
                        $price = explode("EUR", $row_price->nodeValue);
                        $prices[] = $price[1];
                    } //END FOREACH(2)

                    //Guardar todos los enlaces de los productos
                    foreach ($amazon_links as $link ) { //FOREACH(3)
                        if(strpos($link->nodeValue, 'https://www.amazon.es/') !== false)
                            $links[] = $link->nodeValue;
                    } //END FOREACH(3)
                } //END IF(2)
            } //END IF(1)
        } //END FOR(1)  

        //INSERT Y UPDATE PRODUCTOS
        self::comprobarBBDD($names, $prices, $links);

      //  $repository = $this->getDoctrine()->getManager()->getRepository('AppBundle:Producto');
      //  echo $repository->helloworld();
       // self::separateNames($names);
       // $checklist = self::checkIfExists($names);
        return $this->render('default/amazon.html.twig', array(
            'names'=>$names,
            'prices'=>$prices,
            'links'=>$links//,
            //'checklist' => $checklist // !!!!!
            ));
    }

  /*  public function checkIfExists($names){
        //Select de todo lo incluido en la BD
        $productos = $this->getDoctrine()
            ->getRepository('AppBundle:Producto')
            ->findAll();
        $index = 0;
           foreach ($productos as $producto) {
            $nameProducto = $producto->getProducto();
            foreach ($names as $name) {
                //Conversión de los nombres de los productos en una sola "palabra", sin espacios ni guiones
                $nameProducto = trim($nameProducto);
                $nameProducto = str_replace(" ", "-", $nameProducto);
              //  $nameProducto = str_replace("-", "", $nameProducto);
                $name = trim($name);
                $nameEdit = str_replace(" ", "-", $name);
               // $nameEdit = str_replace("-", "", $nameEdit);
                //Comprobación de igualdad en los nombres ya existentes en BD y la página.
                if ($nameProducto != $nameEdit) {
                    similar_text($nameProducto, $nameEdit, $percent);
                    if ($percent > 70 && $percent < 90) {
                       $checklist[] = array(
                            "nombreProductoDB" => $nameProducto,
                            "nombreProducto" => $nameEdit,
                            "porcentaje" => $percent,
                            );
                    }
                    if ($percent > 90){
                        $productoInsert[] = array(
                            "nombreProductoDB" => $nameProducto, 
                            "nombreProducto" => $nameEdit
                            );
                        //array asociativo nameProducto y nameEdit, comprobar si nameEdit existe 2 veces y pasar a modo manual, sino automático
                        //insertBD();//ACABAR!
                    }
                }
                else {
                    //insertDB();//ACABAR!
                }
            }
            $index++;
        }
        //self::comprobarIguales($productoInsert);
        return $checklist;
        
    }*/

    public function comprobarBBDD($names, $prices, $links){
        $productos = $this->getDoctrine()
        ->getRepository('AppBundle:Producto')
        ->findAll();
        $em = $this->getDoctrine()->getManager();
        for ($i=0;$i<count($names);$i++) {
            $semaforo = 0;
            foreach ($productos as $producto) {
                $nameProducto = $producto->getProducto();
                $productoId = $producto->getIdproducto();
                $names[$i] = trim($names[$i]);
                if (strpos($nameProducto, $names[$i]) !== false) {
                    
                    $price = str_replace(".", "", $prices[$i]);
                    $price = str_replace(",", ".", $price);
                    $semaforo = 1;
                    $query=$em->createQuery("update AppBundle:Producto p SET p.precio='".$price."' where p.producto='".$names[$i]."'");
                        $result=$query->getResult();
                        $em->flush();
              }
          }
          if($semaforo==0){
            $price = str_replace(".", "", $prices[$i]);
            $price = str_replace(",", ".", $price);
            $producto = new Producto();
            $producto->setIdcategoriafk(3);
            $producto->setProducto(trim($names[$i]));
            $producto->setPrecio($price);
            $producto->setIdtiendafk(2);
            $producto->setUrl(trim($links[$i]));
            $em->persist($producto);
        }
    }    
    $em->flush();
}

public function separateNames($names){
    $ensamblador = null;
    $fabricante = null;
    $modelo = null;

    foreach ($names as $name) {
        $name = trim($name);
        $nameEdit = str_replace(" ", "-", $name);
        $nameSegmented = explode("-", $nameEdit);
        for ($i=0; $i < count($nameSegmented); $i++) { 
            if (preg_match('(MSI|Asus|Gigabyte|Sapphire|EVGA)', $nameSegmented[$i]) ) {
                $ensamblador = $nameSegmented[$i];
                for ($j=0; $j < count($nameSegmented); $j++) { 
                    if (preg_match('(GeForce|Radeon)', $nameSegmented[$j])) {
                        $fabricante = $nameSegmented[$j];
                        for ($k=0; $k < count($nameSegmented); $k++) { 
                            if (preg_match('(GTX|GT|RX)', $nameSegmented[$k])) {
                                $modelo = $nameSegmented[$k]." ".$nameSegmented[$k+1];
                            }
                        }  
                    }
                }
            }
        }
        $productoFinal[] = array(
            "ensamblador" => $ensamblador,
            "fabricante" => $fabricante,
            "modelo" => $modelo
            );
    }
    print_r($productoFinal);
}

public function comprobarIguales($productoInsert){
    $contador = 0;
    foreach ($productoInsert as $productoCheck) {
        foreach ($productoInsert as $producto) {
            if ($producto['nombreProducto'] == $productoCheck['nombreProducto']) {
                $contador++;
            }
        }
        if ($contador < 2) {
            $productosOk[] = $productoCheck['nombreProducto'];
        }
    }
    echo $productosOk[0];
}

    /**
    * @Route("/updateIguales", name="updateIguales")
    */
    public function updateIgualesAction(Request $request) {
        $graficasSeleccionadas = $request->get('selected');
        foreach ($graficasSeleccionadas as $grafica) {
            echo "aaaaaa>".$grafica."<br>";
        }
    }
}
