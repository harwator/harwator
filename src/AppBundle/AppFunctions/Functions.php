<?php
namespace AppBundle\AppFunctions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class Functions {

	public function descargarImagen($urlImagen, $target, $directorioBase)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $urlImagen);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$imagen = curl_exec($ch);
		curl_close($ch);
		$archivo = @fopen($directorioBase.$target, 'w');
		if($archivo)
		{
		//	echo 'La imagen '.basename($urlImagen).' ha sido descargada a '.$directorioBase.$target;
			@fwrite($archivo, $imagen);
			@fclose($archivo);
		}else{
		//	echo 'La imagen '.basename($urlImagen).' no se ha sido podido descargar';
		}
	}
}

?>