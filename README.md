![Harwator Logo](/web/img/Logo_HW.png)

# Harwator

Harwator está pensado para ser la  herramienta definitiva de comparación de precios online de entre las mejores tiendas de informática tanto nacionales como internacionales.
Y conseguir que la búsqueda de componentes no sea bajo ningún caso un quebradero de cabeza.

### Objetivos
-   Comparación de precios de componentes informáticos.
-   Búsqueda filtrada por diferentes características de productos.
-   Registro de acceso y  acceso al área de usuario.
-   Módulos de recomendacion de harwator a los usuarios  con componentes destacados.
-   Desbloqueo de nuevas funcionalidades una vez que el usuario se ha registrado en nuestro portal, para acceder a la publicación de comentarios,  valoraciones,  lista de los deseos, y carrito de compra.

### Nuevas funcionalidades (En desarrollo)

  - Blog de novedades y nuevas tecnologías.
  - Configurador de ordenador completo con asistente de piezas y componentes por compatibilidad y piezas mejores valoradas por la comunidad harwator.
  - Nuevas fuentes de comparación de precios (Tiendas).

#### Integrantes

  - Roger Pons Pons
  - Álex Sánchez Fernández
  - Nicolás Zabala Iglesias

Nuestro objetivos es dar un mensaje claro a los futuros usuarios de harwator y todos aquellos que deseen adquirir un nuevo ordenador sin gastarse más de lo estrictamente necesario:

> ¿Hasta cuanto estarías dispuesto a pagar por tu nuevo ordenador?


### Estado actual del proyecto

Por motivos de complejidad y contratiempos, nuestro proyecto queda algo lejos de poseer todas estas funcionalidades descritas con anterioridad, nuestra principal idea era obtener un primer producto con una comparación simple pero efectiva de los tres grandes titanes de la venta de productos informáticos a nivel nacional de PC-Componentes, Amazon, e Ebay.

* [Amazon](http://www.amazon.com)  - Amazon
* [PcComponentes](http://www.pccomponentes.com)  - Pc-Componentes

### Instalación

Véase install.md
