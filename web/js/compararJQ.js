var productosIguales = [];
var table2;
$(function(){
	//Selector de etiquetas "td" con la clase especificada
	var rowsProducto1 = $('td.producto1');
	var rowsProducto2 = $('td.producto2');
	var producto1;
	var producto2;
	var showP1 = document.getElementById("producto1"); 
	var showP2 = document.getElementById("producto2");
	var table = document.getElementById("compararTable");
	var button = $('button.submitIguales');
	table2 = document.getElementById("sendProducts");
	
	var i = 0;
	var rowP1;
	var rowP2;
	//Cuando se haga click se añadirá la clase "selected"
	rowsProducto1.on('click', function(e){
		table.style.display = "table";
		rowP1 = $(this);
		rowsProducto1.fadeIn();
		rowsProducto1.removeClass('selected');
		rowP1.addClass('selected');
		rowP1.fadeOut();
		producto1 = rowP1.html();
		showP1.innerHTML = producto1;
	});

	rowsProducto2.on('click', function(e){
		table.style.display = "table";
		rowP2 = $(this);
		rowsProducto2.fadeIn();
		rowsProducto2.removeClass('selected');
		rowP2.addClass('selected');
		rowP2.fadeOut();
		producto2 = rowP2.html();
		showP2.innerHTML = producto2;
	});

	button.on('click', function(e){
		$('td.selected').addClass('hide');
		table2.style.display = "table";
		showP1.innerHTML = "";
		showP2.innerHTML = "";
		productosIguales.push([producto1, producto2]);
		table.style.display = "none";
		i++;
	});


});

function sendSelectedProducts() {
	$.ajax({
		type: 'POST',
		url: '/productosIguales',
		data: {"productos": productosIguales},
		success: function(data) {
			alert(data);
			table2.style.display = "none";
			
		},
		error: function(data) {
			alert("no va");
		}

	});
}
