$(function () {
    $("div.producto").slice(0,10).show();
    $("#loadMore").on('click', function (e) {
        e.preventDefault();
        $("div.producto:hidden").slice(0, 10).slideDown();
        if ($("div.producto:hidden").length == 0) {
            $("#load").fadeOut('slow');
        }
        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 1500);
    });
});

